(function() {
	"use strict";

	angular
		.module("dashApp", [
			"dashApp.core",
			"dashApp.home",
			"dashApp.sources",
			"dashApp.layout"
			])
	;
})();