(function() {
	"use strict";

	angular
		.module("dashApp.core", [
			"ngMaterial",
			"angular-loading-bar"
			])
	;
})();