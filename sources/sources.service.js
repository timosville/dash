(function() {
	"use strict";

	angular
		.module("dashApp.sources")
		.factory("Sources", SourcesService)
	;

	function SourcesService ($http){

		var sources = [];		
		var url = "data/sources.json";

		$http.get(url).success(function(response) {
			var data = response;

			angular.forEach(data, function(obj) {
				sources.push(obj)
			});
		});

		return sources;

	}
})();
